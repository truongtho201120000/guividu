package com.demo.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConf extends WebSecurityConfigurerAdapter{

	@Autowired
	UserDetailsService userDetailsService;
	
	@Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		// TODO Auto-generated method stub
////		http.authorizeRequests()
////		.antMatchers("/user/**","/home").permitAll() //.anyRequest().authenticated() phai duoc chung thuc thi moi duoc vao
////	//	.antMatchers("/admin/**").hasAuthority("ADMIN")
////		.anyRequest().authenticated()
////		.and().formLogin().loginPage("/login").permitAll()
////		.defaultSuccessUrl("/login?success=true")
////		.failureUrl("/login?success=false")
////		.loginProcessingUrl("/perform_login");
//		
//		http.csrf().disable().authorizeRequests() // bat dau cau hinh security
//		//cho phép các request static không bị ràng buộc
//		.antMatchers("/user/**", "/admin/**")
//		.permitAll()
//		//các request kiểu: "/admin/" phải đăng nhập
//		.antMatchers("/admin/**").hasAuthority("admin")
//	//	.antMatchers("/user/home").hasRole("GUEST")
//		.and()
//		//cấu hình trang đăng nhập
//		.formLogin().loginPage("/login").loginProcessingUrl("/j_spring_security_check").defaultSuccessUrl("/home", true)
//		.failureUrl("/login?login_error=true").permitAll()
//		.and()
//		.logout().logoutUrl("/logout").logoutSuccessUrl("/home").invalidateHttpSession(true)
//		.deleteCookies("JSESSIONID").permitAll();
//	}
	@Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // We don't need CSRF for this example
        httpSecurity.csrf().disable()
                // dont authenticate this particular request
                .authorizeRequests().antMatchers("/authenticate", "/register").permitAll()
                .antMatchers("/user/**","/home","/").permitAll();
                
  //             .antMatchers("/admin/**","/api/admin/**").hasAuthority("admin")
   //             .anyRequest().authenticated()
//                .and().formLogin().loginPage("/login").loginProcessingUrl("/j_spring_security_check").defaultSuccessUrl("/home", true)
//        		.failureUrl("/login?login_error=true").permitAll()
//        		.and().logout().logoutUrl("/logout").logoutSuccessUrl("/home").invalidateHttpSession(true)
//        		.deleteCookies("JSESSIONID").permitAll();
                // all other requests need to be authenticated
                // make sure we use stateless session; session won't be used to
                // store user's state.
//                .and().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
////
////        // Add a filter to validate the tokens with every request
//        		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
   }
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
		//super.configure(auth);
//		auth.inMemoryAuthentication().withUser("user1").password(passwordEncoder().encode("123"))
//		.authorities("USER");
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	@Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
