package com.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.demo.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{
//	@Query(value="SELECT * FROM tblEmployee Where id = ?1")
//	public Employee findOneEmployee(long id);
	List<Employee> findByName(String employeeName);
}
