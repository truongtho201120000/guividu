package com.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
	//@Query(value = "select * from UserEntity u where u.username = ?1",nativeQuery = true)
	@Query(value = "SELECT e.* FROM tbl_users e Where e.username = ?1", nativeQuery = true)
	public UserEntity findUserByUsername(String username);
}
