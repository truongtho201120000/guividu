package com.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.entity.CategoryEntity;
import com.demo.entity.ProductEntity;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long>{
//	public CategoryEntity findById(long id);
//	@Query("select id from tbl_category where name =?1")
//	public long findIdByName(String name);
	
	@Query(value = "SELECT e.* FROM tbl_category e Where e.name like %?1%", nativeQuery = true)
	public List<CategoryEntity> findByTitle(String name);
}
