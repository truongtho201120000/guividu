package com.demo.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.entity.RoleEntity;
import com.demo.entity.UserEntity;
import com.demo.service.IRoleService;

@RestController
@RequestMapping("/api/admin")
public class RoleAPI {
	@Autowired
	private IRoleService roleService;
	
	@GetMapping("/roles")
	public List<RoleEntity> getAll(){
		return roleService.findAll();
	}
	
	@PostMapping("/roles")
	public ResponseEntity<RoleEntity> saveRole(@RequestBody RoleEntity roleEntity){
		//Phan nay de add vao bang tbl_users_roles
		List<UserEntity> userList = roleEntity.getUsers();
        userList.forEach(user -> {
            List<RoleEntity> roles = new ArrayList<>();
            roles.add(roleEntity);
            user.setRoles(roles);
        });
//        public void addRoles(RoleEntity role) {
//    		role.getUser().add(this);
//    		roles.add(role);
//    	}
//    	
//    	public void deleteRoles(RoleEntity role) {
//    		role.getUser().remove(this);
//    		roles.remove(role);
//    	}
//        role.setUsers(userList);
        RoleEntity newRole = roleService.saveRole(roleEntity);
        ResponseEntity<RoleEntity> responseEntity = new ResponseEntity<>(newRole, HttpStatus.CREATED);
        return responseEntity;
	}
	
	
	
}
