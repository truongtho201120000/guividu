package com.demo.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.entity.UserEntity;
import com.demo.service.IUserService;

@RestController
@RequestMapping("/api/admin")
public class UserAPI {
	@Autowired
	private IUserService userService;
	
	@GetMapping("/users")
	public List<UserEntity> getAll(){
		return userService.findAll();
	}
	@GetMapping("/username/{name}")
	public UserEntity getAll(@PathVariable String name){
		return userService.loadUserByUserName(name);
	}
	@PostMapping("/users")
	public ResponseEntity<UserEntity> saveUsers(@RequestBody UserEntity userEntity){
		UserEntity user = userService.saveUser(userEntity);
		return new ResponseEntity<UserEntity>(user,HttpStatus.OK);
	}
	
	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable long id) {
		userService.deleteUser(id);
	}
	
	
}
