package com.demo.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.ResponseObjectDTO;
import com.demo.entity.Employee;
import com.demo.service.IEmployeeService;

import net.bytebuddy.implementation.bytecode.assign.primitive.VoidAwareAssigner;

@RestController
public class EmployeeController {

	@Autowired
	private IEmployeeService employeeService;

//	@GetMapping("/employee")
//	public List<Employee> getAll(@RequestBody Employee employee){
//		return employeeService.findAll();
//	}
	@GetMapping("/employee")
	public ResponseEntity<List<Employee>> getAll() {
		return new ResponseEntity<>(employeeService.findAll(), HttpStatus.OK);
	}

//	@GetMapping("/employee/{id}")
//    public ResponseEntity<Employee> getById(@PathVariable long id) throws Exception {
//        Optional<Employee> employee = employeeService.findOneEmployee(id);
//        if (employee.isPresent()) {
//            return new ResponseEntity<>(employee.get(), HttpStatus.OK);
//        } else {
//            throw new Exception("Khong tim thay id: "+id);
//        }
//    }
	@GetMapping("/employee/{id}")
	public ResponseEntity<Employee> getById(@PathVariable long id) {
		try {
			Employee employee = employeeService.findOneEmployee(id);
			return new ResponseEntity<Employee>(employee, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}

	}
//	@GetMapping("/employee/{id}")
//	public ResponseEntity<ResponseObject> getById(@PathVariable long id) {
//		Employee employee = employeeService.findOneEmployee(id);
//		if (employee == null) {
//			return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("200", "Tim thanh cong", employee));
//		} else {
//			return ResponseEntity.status(HttpStatus.NOT_FOUND)
//					.body(new ResponseObject("fail", "Cann't find employee with id = " + id, ""));
//		}
//	}

//	@PostMapping("/employee")
//	public Employee createEmployee(@RequestBody Employee employee){
//		return employeeService.saveEmployee(employee);
//	}
//	@PostMapping("/employee")
//	public ResponseEntity<Employee> create(@RequestBody Employee employee) {
//		Employee employee2 = employeeService.saveEmployee(employee);
//	    if (employee2 == null) {
//	        return null;
//	    } else {
//	        return new ResponseEntity<>(employee2, HttpStatus.CREATED);
//	    }
//	}
//	@PostMapping("/employee")
//	public ResponseEntity<ResponseObject> createEmployee(@RequestBody Employee employee){
//		return ResponseEntity.status(HttpStatus.OK).body(
//				new ResponseObject("200","Insert succesfully",employeeService.saveEmployee(employee))
//				);
//	}
	// Them san phan khong turng ten
	@PostMapping("/employee")
	public ResponseEntity<ResponseObjectDTO> createEmployee(@RequestBody Employee employee) {
		List<Employee> foundEmployeeName = employeeService.findByEmployeeName(employee.getName().trim());
		if (foundEmployeeName.size() > 0) {
			return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
					.body(new ResponseObjectDTO("failed", "Ten da trung", ""));
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObjectDTO("200", "Insert succesfully", employeeService.saveEmployee(employee)));
	}
//@PutMapping("/employee/{id}")
//public Employee updateEmployee(@RequestParam("id") long id, @RequestBody Employee employee) {
//		return iEmployeeService.updateEmployee(id, employee);

//	}
//}

	
	
	@PutMapping("/employee/{id}")
	public ResponseEntity<ResponseObjectDTO> updateEmployee(@RequestBody Employee newEmployee, @PathVariable long id) {
		Employee findEmp = employeeService.findOneEmployee(id);
		if (findEmp == null) {
			return null;
		} else {
			Employee updatedEmployee = employeeService.editEmployee(id, newEmployee);

			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObjectDTO("200", "Update succesfully", updatedEmployee));
		}

	}

//		return ResponseEntity.status(HttpStatus.NOT_FOUND)
//				.body(new ResponseObject("failed", "Khong tim thay id "+id, ""));

//	@DeleteMapping("/employee/{id}")
//	public void deleteEmployee(@PathVariable long id) {
//		 employeeService.deleteEmployee(id);
//	}	
	@DeleteMapping("/employee/{id}")
	public ResponseEntity<ResponseObjectDTO> deleteEmployee(@PathVariable long id) {
		boolean exists = employeeService.existById(id);
		if (exists) {
			employeeService.deleteEmployee(id);
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseObjectDTO("200", "Xoa thanh cong", ""));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObjectDTO("failed", "Khong tim thay id", ""));
	}

}
