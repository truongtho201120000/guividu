package com.demo.service;

import java.util.List;

import com.demo.entity.RoleEntity;

public interface IRoleService {

	public RoleEntity saveRole(RoleEntity roleEntity);
	
	public List<RoleEntity> findAll();
}
