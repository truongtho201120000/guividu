package com.demo.service;

import java.util.List;

import com.demo.entity.UserEntity;

public interface IUserService {
	public UserEntity saveUser(UserEntity user);
	
	public List<UserEntity> findAll();
	
	public void deleteUser(long id);
	
	public UserEntity loadUserByUserName(String username);
}
