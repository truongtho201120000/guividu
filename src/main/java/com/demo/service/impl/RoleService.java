package com.demo.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.entity.RoleEntity;
import com.demo.repository.RoleRepository;
import com.demo.service.IRoleService;

@Service
public class RoleService implements IRoleService{

	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	@Transactional
	public RoleEntity saveRole(RoleEntity roleEntity) {
		// TODO Auto-generated method stub
		return roleRepository.save(roleEntity);
	}

	@Override
	@Transactional
	public List<RoleEntity> findAll() {
		// TODO Auto-generated method stub
		return roleRepository.findAll();
	}

}
