package com.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.demo.entity.UserEntity;
import com.demo.service.IUserService;

@Service
public class UserDetailServiceImpl implements UserDetailsService{
	@Autowired
	private IUserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		//Kiem tra xem user co ton tai trong db
		UserEntity user = userService.loadUserByUserName(username);
		return user;
	}

}
