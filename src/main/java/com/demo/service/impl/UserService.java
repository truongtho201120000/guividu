package com.demo.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.entity.UserEntity;
import com.demo.repository.UserRepository;
import com.demo.service.IUserService;

@Service

public class UserService implements IUserService{
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	@Transactional
	public UserEntity saveUser(UserEntity user) {
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public List<UserEntity> findAll() {
		return userRepository.findAll();
	}

	@Override
	@Transactional
	public void deleteUser(long id) {
		userRepository.deleteById(id);
	}

	@Override
	public UserEntity loadUserByUserName(String username) {
		UserEntity users = userRepository.findUserByUsername(username);
		return users;
	}

}
