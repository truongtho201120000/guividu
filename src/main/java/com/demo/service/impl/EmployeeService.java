package com.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.entity.Employee;
import com.demo.repository.EmployeeRepository;
import com.demo.service.IEmployeeService;

@Service
public class EmployeeService implements IEmployeeService{
	//Khai bao 1 thang noi chuyen 
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Override
	public List<Employee> findAll() {
		// TODO Auto-generated method stub
		return employeeRepository.findAll();
	}

	@Override
	public Employee findOneEmployee(long id) {
		// TODO Auto-generated method stub
		Employee foundEmployee = employeeRepository.findById(id).get();
		if(foundEmployee != null) {
			return foundEmployee;
		}
		return null;
	}
	@Override
	public Employee saveEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return employeeRepository.save(employee);
	}

//	@Override
//	public Employee editEmployee(long id, Employee employee) {
//		// TODO Auto-generated method stub
//	//	Employee oldEmployee = employeeRepository.findOneEmployee(id);
////		if(oldEmployee != null) {
////			oldEmployee.setName()
////			if(oldEmployee != null) {
////				oldEmployee.setName(employee.getName());
////				oldEmployee.setAddress(employee.getAddress());
////				return employeeRepository.save(oldEmployee);
//////			}
////		}
//		return null;
//	}
	@Override
	public Employee editEmployee(long id, Employee employee) {
		// TODO Auto-generated method stub
		if(employee != null) {
			Employee oldEmployee = employeeRepository.findById(id).get();
			if(oldEmployee != null) {
				oldEmployee.setName(employee.getName());
				oldEmployee.setAddress(employee.getAddress());
				return employeeRepository.save(oldEmployee);
			}
		}
		return null;
}

	@Override
	public boolean deleteEmployee(long id) {
		// TODO Auto-generated method stub
		if(id >=1) {
			employeeRepository.deleteById(id);
			return true;
		}
		return false;
		
	}

	@Override
	public List<Employee> findByEmployeeName(String name) {
		// TODO Auto-generated method stub
		return employeeRepository.findByName(name);
	}

	@Override
	public boolean existById(long id) {
		// TODO Auto-generated method stub
		return employeeRepository.existsById(id);
	}

	

}
