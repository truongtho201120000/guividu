<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!-- directive cua JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>ListProduct - SB Admin</title>
        
        <!-- VARIABLES -->
		<jsp:include page="/WEB-INF/views/common/variables.jsp"></jsp:include>
		
		<!-- CSS -->
		<jsp:include page="/WEB-INF/views/admin/layout/css.jsp"></jsp:include>
    </head>
    <body class="sb-nav-fixed">
        <!-- HEADER -->
        <jsp:include page="/WEB-INF/views/admin/layout/header.jsp"></jsp:include>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <!-- MENU -->
                <jsp:include page="/WEB-INF/views/admin/layout/menu.jsp"></jsp:include>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                       <!--  <h1 class="mt-4">Product</h1> -->
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="${base}/admin/index">Dashboard</a></li>
                            <li class="breadcrumb-item active">Product / List-Product</li>
                        </ol>
                        
                        <h3 align="center">Danh sách product</h3>
                <form>
                      	<label>Tìm kiếm sản phẩm: </label>
                        <input type="text" name="name" id="name"/> <!-- Keyword lấy theo parameter  -->
                        <button type="button" class="btn btn-info" id="btn-submit" onclick="getProductByTitle()">Search</button>
                        
                  </form>
                  	<div id="getResultDiv" style="padding: 20px 10px 20px 50px">
						
					</div>
                        <form>
                        <a href="${base }/admin/add-product" class="btn btn-primary">Add New Product</a>
                        <p>
                        	<!-- Để dùng đc jstl thì phải include nó vào -->
                        	<table class="table table-striped table-dark">
								  <thead>
								    <tr>
								      <th scope="col">ID</th>
								      <th scope="col">Title</th>
								      <th scope="col">Price</th>
								      <th scope="col">PriceSale</th>
								      <th scope="col">ShortDescription</th>
								      <!-- <th scope="col">Detail</th> -->
								      <th scope="col">Avatar</th>
								      <th scope="col">CategoryName</th>
								      <th scope="col">Action</th>
								    </tr>
								  </thead>
								  <c:forEach var="product" items="${listProduct }">
                        			<!-- items ở đây là biến mà ta đẩy từ thằng controller -->
                        		<%-- <p>${category.name }</p>
                        		<p>${category.description }</p> --%>
                        	
								  <tbody id="load_data">
								    <%-- <tr id="duLieuBanDau">
								      <th scope="row">${product.id }</th>
								      <td>${product.title }</td>
								      <td>${product.price }</td>
								      <td>${product.priceSale }</td>
								      <td>${product.shortDescription }</td>
								      <td>${product.details }</td>
								      <td>
								      		<img alt="" width="50px" src="${base }/upload/${product.avatar}" />
								      </td>
								      <td>${product.categories.name }</td>
								      <td>
								      	
								      	<a href="${base }/admin/delete/${product.seo}" class="btn btn-danger">Delete</a>
								      	<a href="${base }/admin/edit-product/${product.seo }" class="btn btn-success">Edit</a>
								      </td>
								    </tr> --%>
								    
								  </tbody>
								  </c:forEach>
								</table>
                        		<nav aria-label="Page navigation example">
								  <ul class="pagination">
								   
								  </ul>
								</nav>
                        </p>
                        </form>
                    </div>
                    <!-- Button trigger modal -->
<!-- Modal EDIT-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin:30px 60px">
    <div class="modal-content" style="width:1200px;">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Edit product</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form >
        <input type="hidden" id="idProduct">
        <div class="col-md-12 ">
        <!-- <form class="register-form" role="form"> -->
            <div class="form-group">
            <label class="info-category" for="exampleInputCategory">Category (required)<span></span></label>
			<select name="category" class="form-control" id="category">
					<c:forEach var="cate" items="${listCategory }">
						<option value="${cate.id}">${cate.name}</option>
					</c:forEach>
			</select>
			<!-- <select id="load_data" style="width: 100%;height: 40px;border: 1px solid #eee;border-radius: 5px; outline: none;">
				
			</select> -->
          </div>
        <!-- </form> -->
        </div>
        <div class="col-md-12 ">
        <!-- <form class="register-form" role="form"> -->
            <div class="form-group">
            <label class="info-title" for="exampleInputTitle">Title (required) <span></span></label>
            <input  type="text" class="form-control unicase-form-control text-input" id="exampleInputTitle" placeholder="title"/>
          </div>
        <!-- </form> -->
    </div>
    <div class="col-md-12 ">
        <!-- <form class="register-form" role="form"> -->
            <div class="form-group">
            <label class="info-title" for="exampleInputPrice">Price (required) </label>
            <input  type="number" class="form-control unicase-form-control text-input" id="exampleInputPrice" placeholder="Price"/>
          </div>
        <!-- </form> -->
    </div>
    <div class="col-md-12">
        <div class="register-form" role="form">
            <div class="form-group">
            <label class="info-title" for="exampleInputEmail1">Price sale (required) </label>
            <input  type="text" class="form-control unicase-form-control text-input" id="exampleInputPriceSale" placeholder="Price sale"/>
          </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="register-form" role="form">
            <div class="form-group">
            <label class="info-title" for="exampleInputTitle">Description (required) </label>
            <textarea  class="form-control unicase-form-control" id="exampleInputDescription" placeholder="Short Description"></textarea>
          </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="register-form" role="form">
            <div class="form-group">
            <label class="info-title" for="exampleInputDetail">Detail (required) </label>
            <%-- <sf:textarea path="details" class="form-control unicase-form-control" id="exampleInputDetail" placeholder="Mô tả chi tiết vấn đề của bạn"></sf:textarea> --%>
            <!-- <div id="summernote">Summernote</div> -->
             <textarea id="summernote" name="editordata"></textarea>
          </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="register-form" role="form">
            <div class="form-group">
                <input type="checkbox"  name="exampleInputCheckbox" id="exampleInputCheckbox"/>
                <label class="info-title" for="exampleInputCheckbox">Is Hot Product? </label>
            
          </div>
        </div>
    </div>
    <div class="col-md-12">
    <div class="form-group">
        <label for="inputFile">Avatar</label>
        <input id="inputFile" name="inputAvatar" type="file" class="form-control-file" >
    </div>
    </div>
    <div class="col-md-12">
    <div class="form-group">
        <label for="inputFile">Picture(Multiple)</label>
        <input id="inputFile" name="inputPictures" type="file" class="form-control-file" multiple="multiple">
    </div>
    </div>
    <%-- <div class="col-md-12 outer-bottom-small m-t-20">
    	<a href="${base }/admin/list-product" class="btn-upper btn btn-secondary checkout-page-button">Back To List</a>
        <a href="${base }/admin/list-product"><input type="button"  class="btn-upper btn btn-primary checkout-page-button"  value="Gui"/></a>
        <button type="button" id="btn-gui" class="btn-upper btn btn-primary checkout-page-button" onclick="addProduct()">Gui</button>
    </div> --%>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="btnEditChange()">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal DELETE-->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Thong bao</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" id="idDelete">
        Ban co muon xoa san pham nay khong?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="deletePro()">OK</button>
      </div>
    </div>
  </div>
</div>
                </main>
                <!-- Footer -->
                <jsp:include page="/WEB-INF/views/admin/layout/footer.jsp"></jsp:include>
            </div>
        </div>
        <!-- JAVASCRIPT -->
        <jsp:include page="/WEB-INF/views/admin/layout/js.jsp"></jsp:include>
       
</body>

       <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
		<script type="text/javascript">
			//$('#summernote').summernote();
			$(document).ready(function() {
			    $('#summernote').summernote({
			      placeholder: 'Enter content....',
			      tabsize: 2,
			      height: 100,
			      minHeight: 100,
			      maxHeight: 300,
			      focus: true,
			      
			    });
			  
			});
		</script>
 <script>
        $(document).ready(function() {
            //dòng này minh dùng để load data
            LoadData(1);
        //hàm để load data
        // ở đây mình có sử dụng dùng công chuỗi các tr khi chạy qua từng bản ghi
        // 
        function LoadData(page) {
            $.ajax({
                url: '/api/admin/pageProduct?pageNo='+page,
                type: 'GET',
                success: function(rs) {
                    var str = "";
                    $.each(rs.content,function(i, product) {
                    	str +="<tr>";
						str +="<td>"+product.id + "</td>";
						str +="<td>"+product.title + "</td>";
						str +="<td>"+product.price + "</td>";
						str +="<td>"+product.priceSale + "</td>";
						str +="<td>"+product.shortDescription + "</td>";
						str +="<td>"+product.avatar + "</td>";
						str +="<td>"+product.categories.name + "</td>";
						/* str +="<td>"	
				      		<>Delete</a href="${base }/admin/edit-product/${product.seo }">
				      		<a href="${base }/admin/edit-product/${product.seo }" class="btn btn-success">Edit</a>
				      		"</td>"; */
				      	str += "<td><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal' onclick='editProduct("+product.id+")'>Edit</button><button type='button' class='btn btn-danger' data-toggle='modal' data-target='#exampleModal2' onclick='deleteProduct("+product.id+")'>Delete</button></td>";
						str +="</tr>";
								/* $("#getResultDiv").append(
										product.title,product.price); */
							});
                    // id của thẻ tbody để hiển thị tất cả cá tr ra
                    $('#load_data').html(str);
                    if(rs.totalPages > 1 ){
						for(var numberPage = 1; numberPage <= rs.totalPages; numberPage++) {
							var li = '<li class="page-item "><a class="pageNumber" style="padding-right:5px" >'+numberPage+'</a></li>';
						    $('.pagination').append(li);
						};				
						
						// active page pagination
				    	$(".pageNumber").each(function(index){	
				    		if($(this).text() == page){
				    			$(this).parent().removeClass().addClass("page-item active");
				    		}
				    	});
               		 }
            });
        }
        };
     })
    </script>   -->
    
<!-- <script type="text/javascript">
	$(document).ready(function() {
		LoadData(1);
		
		function LoadData(page) {
			//var name = $("#name").val();
		//	alert(name);
			$.ajax({
				url : '/api/admin/pageProduct?pageNo='+page,
				type : "GET",
				success : function(rs) {
					var str = "";
					$.each(rs.content,function(i, product) {
						str +="<tr>";
						str +="<td>"+product.id + "</td>";
						str +="<td>"+product.title + "</td>";
						str +="<td>"+product.price + "</td>";
						str +="<td>"+product.priceSale + "</td>";
						str +="<td>"+product.shortDescription + "</td>";
						str +="<td>"+product.avatar + "</td>";
						str +="<td>"+product.categories.name + "</td>";
						/* str +="<td>"	
				      		<>Delete</a href="${base }/admin/edit-product/${product.seo }">
				      		<a href="${base }/admin/edit-product/${product.seo }" class="btn btn-success">Edit</a>
				      		"</td>"; */
				      	str += "<td><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal' onclick='editProduct("+product.id+")'>Edit</button><button type='button' class='btn btn-danger' data-toggle='modal' data-target='#exampleModal2' onclick='deleteProduct("+product.id+")'>Delete</button></td>";
						str +="</tr>";
									});
					// id của thẻ tbody để hiển thị tất cả cá tr ra
					$('#load_data').html(str);
					if(rs.totalPages > 1 ){
						for(var numberPage = 1; numberPage <= rs.totalPages; numberPage++) {
							var li = '<li class="page-item "><a class="pageNumber" style="padding-right:5px" >'+numberPage+'</a></li>';
						    $('.pagination').append(li);
						};				
						
						// active page pagination
				    	$(".pageNumber").each(function(index){	
				    		if($(this).text() == page){
				    			$(this).parent().removeClass().addClass("page-item active");
				    		}
				    	});
					};
				}
			});
		}
		$(document).on('click', '.pageNumber', function (event){
//			event.preventDefault();
			var page = $(this).text();	
	   // 	$('.danhMucTable tbody tr').remove();
	    	$('.pagination li').remove();
	    	LoadData(page);	
		});
	})
</script>   --> 
 
<!-- <script type="text/javascript">
	 /* $(document).ready(
		function() {
			// GET REQUEST
			$("#btn-submit").click(function(event) {
				event.preventDefault();
				getProductByTitle();
			});  */

			// DO GET
			function getProductByTitle() {
				var ten = $("#name").val();
				/* alert(ten); */
				$.ajax({
					type : "GET",
					url : "/api/admin/findProduct?name="+ten,
					success : function(result) {
					/* 	alert("success"); */
						var str = "";
						$.each(result, function(i, product) {
									str +="<tr>";
									str +="<td>"+product.id + "</td>";
									str +="<td>"+product.title + "</td>";
									str +="<td>"+product.price + "</td>";
									str +="<td>"+product.priceSale + "</td>";
									str +="<td>"+product.shortDescription + "</td>";
									str +="<td>"+product.avatar + "</td>";
									str +="<td>"+product.categories.name + "</td>";
									str += "<td><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal' onclick='editProduct("+product.id+")'>Edit</button><button type='button' class='btn btn-danger' data-toggle='modal' data-target='#exampleModal2' onclick='deleteProduct("+product.id+")'>Delete</button></td>";
									str +="</tr>";
									/* $("#getResultDiv").append(
											product.title,product.price); */
								});
						$('#load_data').html(str);
						/* $("#duLieuBanDau").css("display", "none"); */
						/* console.log("Success: ", result); */
					},
					error : function(e) {
						console.log("ERROR: ", e);
					}
				});
			}
		/*  })  */
</script> -->
<!-- <script type="text/javascript">
	function editProduct(id) {
		
		$.get("http://localhost:8993/api/admin/product/"+id, function(data, status){
		//	let e = document.getElementById(“category”);
		//	let giaTri = e.options[e.selectedIndex].text;
			var cate = document.getElementById("category");
			$("#idProduct").val(id);
			$("#exampleInputTitle").val(data.title);
			$("#exampleInputPrice").val(data.price);
			$("#exampleInputPriceSale").val(data.priceSale);
			$("#exampleInputDescription").val(data.shortDescription);
		//	$("#summernote").val(data.details);
			$('#summernote').summernote('code', data.details);
		//	$("#category").text(data.categories.name);
		//	$("#category").options[$("#category").selectedIndex].text = data.categories.name;
		//	e.options[e.selectedIndex].text = data.categories.name
		// 	$("#category" ).text( data.categories.name);
		// 	$("#category").selectedIndex = data.categories.id;
			cate.options[cate.selectedIndex].text = data.categories.name;
		//	alert("category:"+data.categories.name);
		});
	}
</script>

<script type="text/javascript">
	function btnEditChange() {
		var id = $("#idProduct").val();
		let data = {
				title: $("#exampleInputTitle").val(),
				price: $("#exampleInputPrice").val(), // lay theo id
				priceSale: $("#exampleInputPriceSale").val(), // lay theo id
				shortDescription: $("#exampleInputDescription").val(),
				details: $("#summernote").val(),
				categories: {id: $("#category").val()}
			};
			// $ === jQuery
			// json == javascript object
			$.ajax({
				url : "/api/admin/product/"+id,
				type : "PUT",
				contentType : "application/json",
				data : JSON.stringify(data),

				dataType : "json", // kieu du lieu tra ve tu controller la json
				success : function(result) {
					//alert("Chuc mung! da luu thanh cong dia chi email " + jsonResult.message.txtLuaChon);
					alert("Sua san pham thanh cong");
					window.location.href = "http://localhost:8993/admin/list-product";
				},
				error : function(jqXhr, textStatus, errorMessage) { // error callback 
					alert("Loi sua san pham");
				}
			});
	}
</script> -->

<!-- <script type="text/javascript">
	function deleteProduct(id) {
			$("#idDelete").val(id);
	};
	function deletePro() {
		var id = $("#idDelete").val();
	//	alert(id);
			$.ajax({
				url : "/api/admin/product/"+id,
				type : "DELETE",
				contentType : "application/json",
	//			data : JSON.stringify(data),
				dataType : "json", // kieu du lieu tra ve tu controller la json
				success : function(result) {
					//alert("Chuc mung! da luu thanh cong dia chi email " + jsonResult.message.txtLuaChon);
					alert("Xoa thanh cong");
				//	$("#exampleModalCenter").modal();
					
					window.location.href = "http://localhost:8993/admin/list-product";
					
				},
				error : function(jqXhr, textStatus, errorMessage) { // error callback 
					alert("Xoa that bai");
					
				}
			});
	};
</script> -->
<!--     <script type="text/javascript">
    $(document).ready(
    		function() {
    	$("#btn-submit").click(function(event) {
			event.preventDefault();
			getProductByTitle();
		});
    
    	function getProductByTitle() {
			var ten = $("#name").val();
			alert(ten);
			$.ajax({
				type : "GET",
				url : "/api/admin/findProduct?name="+ten,
				success: function(result){
					console.log(1);
					},
				error: function(){
					console.log(2);
				}
			});	
		}})
    </script> -->
</html>
	