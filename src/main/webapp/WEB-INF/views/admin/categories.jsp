<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!-- directive cua JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="" />
<meta name="author" content="" />
<title>Category - SB Admin</title>
<!-- VARIABLES -->
<jsp:include page="/WEB-INF/views/common/variables.jsp"></jsp:include>
    <link href="${base }/admin/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
<!-- CSS -->
<jsp:include page="/WEB-INF/views/admin/layout/css.jsp"></jsp:include>

</head>
<body class="sb-nav-fixed">
	<!-- HEADER -->
	<jsp:include page="/WEB-INF/views/admin/layout/header.jsp"></jsp:include>
	<div id="layoutSidenav">
		<div id="layoutSidenav_nav">
			<!-- MENU -->
			<jsp:include page="/WEB-INF/views/admin/layout/menu.jsp"></jsp:include>

		</div>
		<div id="layoutSidenav_content">
			<main>
				<div class="container-fluid">

					<ol class="breadcrumb mb-4">
						<li class="breadcrumb-item"><a href="${base}/admin/index">Dashboard</a></li>
						<li class="breadcrumb-item active">Category / List-Category</li>
					</ol>

					<h3 align="center">Danh sách category</h3>
					<form>
						<label>Tìm kiếm danh mục: </label> <input type="text"
							name="name" id="name" />
						<!-- Keyword lấy theo parameter  -->
						<button type="button" class="btn btn-info" onclick="search()">Search</button>
					</form>
					<a href="${base }/admin/add-category" class="btn btn-primary">Add
						New Category</a>
					<p>
						<!-- Để dùng đc jstl thì phải include nó vào -->
					<table class="table table-striped table-dark">
						<thead>
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Name</th>
								<th scope="col">Description</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<%-- <c:forEach var="category" items="${categories }"> --%>
						<!-- items ở đây là biến mà ta đẩy từ thằng controller -->
						<%-- <p>${category.name }</p>
                        		<p>${category.description }</p> --%>

						<tbody id="load_data">
							<%-- <tr>
								      <th scope="row">${category.id }</th>
								      <td>${category.name }</td>
								      <td>${category.description }</td>
								      <td>
								      	<a href="${base }/admin/del-category/${category.id}" class="btn btn-danger">Delete</a>
								      	<a href="${base }/admin/edit-category/${category.id}" class="btn btn-success">Edit</a>
								      </td>
								    </tr> --%>
							<!-- Button trigger modal -->
							<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button> -->


						</tbody>
						<%--  </c:forEach> --%>
					</table>
					<ul class="pagination">
	  				</ul>
					<!-- Modal BUTTON EDIT-->
					<div class="modal fade" id="exampleModalCenter" tabindex="-1"
						role="dialog" aria-labelledby="exampleModalCenterTitle"
						aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLongTitle">Edit
										Category</h5>
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<input type="hidden" id="custId">
									<div class="form-group">
										<label for="name">Tên danh mục:</label> 
										<input type="text" class="form-control" id="tenDanhMuc" placeholder="Nhập vào tên danh mục" required />
									</div>
									<div class="form-group">
										<label for="desc">Description:</label>
										 <input type="text" class="form-control" id="moTaDanhMuc" />
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary"
										data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary" onclick="saveChange()">Save
										changes</button>
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="margin-right: 10px; margin-left: 10px">
						<p class="demo demo4_bottom"></p>
					</div>
					<%-- <nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item"><a class="page-link" href="#">Previous</a></li>
							<c:forEach items="${listPage}" var="p">
								<li class="page-item"><a class="page-link"
									href="${base }/admin/categories?page=${p }">${p }</a></li>
							</c:forEach>

							<li class="page-item"><a class="page-link" href="#">Next</a></li>
						</ul>
					</nav> --%>
					</p>
					<!--     </form> -->
				</div>
				<!-- Button trigger modal -->


<!-- Modal Delete-->
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Xoa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" id="custIdDelete">
        Ban co muon xoa danh muc nay khong?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="deleteCate()">OK</button>
      </div>
    </div>
  </div>
</div>
			</main>

			<!-- Footer -->
			<jsp:include page="/WEB-INF/views/admin/layout/footer.jsp"></jsp:include>
		</div>
	</div>
	<!-- JAVASCRIPT -->
	<jsp:include page="/WEB-INF/views/admin/layout/js.jsp"></jsp:include>
</body>
<!-- <script type="text/javascript">
	$(document).ready(function() {
		//dòng này minh dùng để load data
		LoadData(1);
	//hàm để load data
	// ở đây mình có sử dụng dùng công chuỗi các tr khi chạy qua từng bản ghi
	// 
	function LoadData(page) {
		var totalStudent = "${totalCategory}";
	//	alert("totalStudent: "+totalStudent);
		$.ajax({
			//		url : "/api/admin/category/"+page+" /10,
					url: "/api/admin/categoryPage?page="+page,
					type : 'GET',
					success : function(rs) {
						var str = "";
						$.each(rs.content,function(i, cate) {
											str += "<tr>";
											str += "<td id='idCate'>" + cate.id + "</td>";
											str += "<td>" + cate.name + "</td>";
											str += "<td>" + cate.description
													+ "</td>";
											str += "<td><button id='btn-edit' type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModalCenter' onclick='editCategory("+ cate.id +")'>Edit</button><button id='btn-delete' type='button' class='btn btn-danger' data-toggle='modal' data-target='#exampleModalCenter2' onclick='deleteCategory("+ cate.id +")'>Delete</button></td>"
											str += "</tr>";
										});
						// id của thẻ tbody để hiển thị tất cả cá tr ra
						$('#load_data').html(str);
					},
					error: function(e){
						alert("Lay du lieu that bai");
					}
				});
		}
	})
}}
</script> -->

<script type="text/javascript">
	$(document).ready(function() {
		LoadData(1);
		
		function LoadData(page) {
			//var name = $("#name").val();
		//	alert(name);
			$.ajax({
				url : "/api/admin/categoryPage?pageNo="+page,
				type : "GET",
				success : function(rs) {
					var str = "";
					$.each(rs.content,function(i, cate) {
										str += "<tr>";
										str += "<td id='idCate'>" + cate.id + "</td>";
										str += "<td>" + cate.name + "</td>";
										str += "<td>" + cate.description +"</td>";
										str += "<td><button id='btn-edit' type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModalCenter' onclick='editCategory("+ cate.id +")'>Edit</button><button id='btn-delete' type='button' class='btn btn-danger' data-toggle='modal' data-target='#exampleModalCenter2' onclick='deleteCategory("+ cate.id +")'>Delete</button></td>"
										str += "</tr>";
									});
					// id của thẻ tbody để hiển thị tất cả cá tr ra
					$('#load_data').html(str);
					if(rs.totalPages > 1 ){
						for(var numberPage = 1; numberPage <= rs.totalPages; numberPage++) {
							var li = '<li class="page-item "><a class="pageNumber" style="padding-right:5px" >'+numberPage+'</a></li>';
						    $('.pagination').append(li);
						};				
						
						// active page pagination
				    	$(".pageNumber").each(function(index){	
				    		if($(this).text() == page){
				    			$(this).parent().removeClass().addClass("page-item active");
				    		}
				    	});
					};
				}
			});
		}
		$(document).on('click', '.pageNumber', function (event){
//			event.preventDefault();
			var page = $(this).text();	
	   // 	$('.danhMucTable tbody tr').remove();
	    	$('.pagination li').remove();
	    	LoadData(page);	
		});
	})
</script>

<script type="text/javascript">
	function editCategory(cateId) {
	//	var id = $("#idCate").val();
	//	var text= $(this).parent().find('input').val();
	//	alert(cateId);
		$.get("http://localhost:9997/api/admin/category/"+cateId, function(data, status){
			$("#tenDanhMuc").val(data.name);
			$("#moTaDanhMuc").val(data.description);
			$("#custId").val(cateId);
		});
		
	}
</script>
<script type="text/javascript">
	function saveChange() {
		var id = $("#custId").val();
	//	alert(id);
		let data = {
				name: $("#tenDanhMuc").val(),
				description: $("#moTaDanhMuc").val(), // lay theo id
			};
			// $ === jQuery
			// json == javascript object
			$.ajax({
				url : "/api/admin/category/"+id,
				type : "PUT",
				contentType : "application/json",
				data : JSON.stringify(data),
				dataType : "json", // kieu du lieu tra ve tu controller la json
				success : function(result) {
					//alert("Chuc mung! da luu thanh cong dia chi email " + jsonResult.message.txtLuaChon);
					alert("Sua thanh cong");
				//	$("#exampleModalCenter").modal();
					
					window.location.href = "http://localhost:9997/admin/categories";
				},
				error : function(jqXhr, textStatus, errorMessage) { // error callback 
					alert("Sua that bai");
					
				}
			});
	}
</script>

<script type="text/javascript">
	function deleteCategory(cateId) {
	//	var id = $("#idCate").val();
	//	var text= $(this).parent().find('input').val();
	//	alert(cateId);
		$.get("http://localhost:9997/api/admin/category/"+cateId, function(data, status){
			$("#custIdDelete").val(cateId);
		});
		
	}
</script>

<script type="text/javascript">
	function deleteCate() {
		var id = $("#custIdDelete").val();
	//	alert(id);
			$.ajax({
				url : "/api/admin/category/"+id,
				type : "DELETE",
				contentType : "application/json",
	//			data : JSON.stringify(data),
				dataType : "json", // kieu du lieu tra ve tu controller la json
				success : function(result) {
					//alert("Chuc mung! da luu thanh cong dia chi email " + jsonResult.message.txtLuaChon);
					alert("Xoa thanh cong");
				//	$("#exampleModalCenter").modal();
					
					window.location.href = "http://localhost:9997/admin/categories";
					
				},
				error : function(jqXhr, textStatus, errorMessage) { // error callback 
					alert("Xoa that bai");
					
				}
			});
	}
</script>

<script type="text/javascript">
	function search() {
		var name = $("#name").val();
	//	alert(name);
		$.ajax({
			url : "/api/admin/getCategoryByName?name="+name,
			type : "GET",
			success : function(rs) {
				var str = "";
				$.each(rs,function(i, cate) {
									str += "<tr>";
									str += "<td id='idCate'>" + cate.id + "</td>";
									str += "<td>" + cate.name + "</td>";
									str += "<td>" + cate.description
											+ "</td>";
									str += "<td><button id='btn-edit' type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModalCenter' onclick='editCategory("+ cate.id +")'>Edit</button><button id='btn-delete' type='button' class='btn btn-danger' data-toggle='modal' data-target='#exampleModalCenter2' onclick='deleteCategory("+ cate.id +")'>Delete</button></td>"
									str += "</tr>";
								});
				// id của thẻ tbody để hiển thị tất cả cá tr ra
				$('#load_data').html(str);
			}
		});
	}
</script>
<!-- <script>
	$(document).ready(function() {
		set_paging(Math.ceil(totalStudent / 10));
		fetchData(1);
	});

	$('.demo4_bottom').bootpag({
		total : Math.ceil(totalStudent / 10),
		maxVisible : 5,
		page : 1
	});

	function set_paging(total_pages) {
		$('.demo4_bottom').html('');
		$('.demo4_bottom').bootpag({
			total : total_pages,
			page : 1,
			maxVisible : 5,
			leaps : true,
			firstLastUse : true,
			first : '<span aria-hidden="true">&larr;</span>',
			last : '<span aria-hidden="true">&rarr;</span>',
			wrapClass : 'pagination',
			activeClass : 'active',
			disabledClass : 'disabled',
			nextClass : 'next',
			prevClass : 'prev',
			lastClass : 'last',
			firstClass : 'first'
		}).on("page", function(event, num) {
			display_Start = num;
			fetchData(display_Start);
		}).find('.pagination');
	}
</script> -->
</html>
