<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	<jsp:include page="/WEB-INF/views/common/variables.jsp"></jsp:include>
	<!-- directive cua JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<!-- directive SPRING-FORM -->
	<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Add-Product - SB Admin</title>
        <!-- VARIABLES -->
		<jsp:include page="/WEB-INF/views/common/variables.jsp"></jsp:include>
		
		<!-- CSS -->
		<jsp:include page="/WEB-INF/views/admin/layout/css.jsp"></jsp:include>
    </head>
    <body class="sb-nav-fixed">
        <!-- HEADER -->
        <jsp:include page="/WEB-INF/views/admin/layout/header.jsp"></jsp:include>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <!-- MENU -->
                <jsp:include page="/WEB-INF/views/admin/layout/menu.jsp"></jsp:include>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Add - Product</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="${base}/admin/index">Dashboard</a></li>
                            <li class="breadcrumb-item active">Product / Add-Product</li>
                        </ol>
    <div class="col-md-9 contact-form">
    
    <form >
    
    	<%-- <sf:hidden path="id"/> --%>
        <div class="col-md-12 ">
        <!-- <form class="register-form" role="form"> -->
            <div class="form-group">
            <label class="info-category" for="exampleInputCategory">Category (required)<span></span></label>
            <%-- <sf:select path="categories.id" name="" id="" style="width: 100%;height: 40px;border: 1px solid #eee;border-radius: 5px; outline: none;">
                <sf:options value ="${categories }" itemValue ="id" itemLabel ="name"/>
               <
            </sf:select> --%>
            <%-- <sf:select path="categories.id" class="form-control" id="category">
       			<sf:options items="${categories }" itemValue="id" itemLabel="name" />
			</sf:select> --%>
			<select name="category" class="form-control" id="category">
					<c:forEach var="cate" items="${listCategory }">
						<option value="${cate.id}">${cate.name}</option>
					</c:forEach>
			</select>
			<!-- <select id="load_data" style="width: 100%;height: 40px;border: 1px solid #eee;border-radius: 5px; outline: none;">
				
			</select> -->
          </div>
        <!-- </form> -->
        </div>
        <div class="col-md-12 ">
        <!-- <form class="register-form" role="form"> -->
            <div class="form-group">
            <label class="info-title" for="exampleInputTitle">Title (required) <span></span></label>
            <input  type="text" class="form-control unicase-form-control text-input" id="exampleInputTitle" placeholder="title"/>
          </div>
        <!-- </form> -->
    </div>
    <div class="col-md-12 ">
        <!-- <form class="register-form" role="form"> -->
            <div class="form-group">
            <label class="info-title" for="exampleInputPrice">Price (required) </label>
            <input  type="number" class="form-control unicase-form-control text-input" id="exampleInputPrice" placeholder="Price"/>
          </div>
        <!-- </form> -->
    </div>
    <div class="col-md-12">
        <div class="register-form" role="form">
            <div class="form-group">
            <label class="info-title" for="exampleInputEmail1">Price sale (required) </label>
            <input  type="text" class="form-control unicase-form-control text-input" id="exampleInputPriceSale" placeholder="Price sale"/>
          </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="register-form" role="form">
            <div class="form-group">
            <label class="info-title" for="exampleInputTitle">Description (required) </label>
            <textarea  class="form-control unicase-form-control" id="exampleInputDescription" placeholder="Short Description"></textarea>
          </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="register-form" role="form">
            <div class="form-group">
            <label class="info-title" for="exampleInputDetail">Detail (required) </label>
            <%-- <sf:textarea path="details" class="form-control unicase-form-control" id="exampleInputDetail" placeholder="Mô tả chi tiết vấn đề của bạn"></sf:textarea> --%>
            <!-- <div id="summernote">Summernote</div> -->
             <textarea id="summernote" name="editordata"></textarea>
          </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="register-form" role="form">
            <div class="form-group">
                <input type="checkbox"  name="exampleInputCheckbox"/>
                <label class="info-title" for="exampleInputCheckbox">Is Hot Product? </label>
            
          </div>
        </div>
    </div>
    <div class="col-md-12">
    <div class="form-group">
        <label for="inputFile">Avatar</label>
        <input id="inputFile" name="inputAvatar" type="file" class="form-control-file" >
    </div>
    </div>
    <div class="col-md-12">
    <div class="form-group">
        <label for="inputFile">Picture(Multiple)</label>
        <input id="inputFile" name="inputPictures" type="file" class="form-control-file" multiple="multiple">
    </div>
    </div>
    <div class="col-md-12 outer-bottom-small m-t-20">
    	<a href="${base }/admin/list-product" class="btn-upper btn btn-secondary checkout-page-button">Back To List</a>
        <%-- <a href="${base }/admin/list-product"><input type="button"  class="btn-upper btn btn-primary checkout-page-button"  value="Gui"/></a> --%>
        <button type="button" id="btn-gui" class="btn-upper btn btn-primary checkout-page-button" onclick="addProduct()">Gui</button>
    </div>
    </form>
</div>
                        
                        
</div>
</main>
             <!-- FOOTER -->
        	<jsp:include page="/WEB-INF/views/admin/layout/footer.jsp"></jsp:include>
            </div>
        </div>
        <!-- JAVASCRIPT -->
        <jsp:include page="/WEB-INF/views/admin/layout/js.jsp"></jsp:include>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
		<script type="text/javascript">
			//$('#summernote').summernote();
			$(document).ready(function() {
			    $('#summernote').summernote({
			      placeholder: 'Enter content....',
			      tabsize: 2,
			      height: 200,
			      minHeight: 100,
			      maxHeight: 300,
			      focus: true,
			      
			    });
			  
			});
		</script>
    </body>
    <!-- <script>
        $(document).ready(function() {
            //dòng này minh dùng để load data
            LoadCategory();
        });

        //hàm để load data
        // ở đây mình có sử dụng dùng công chuỗi các tr khi chạy qua từng bản ghi
        // 
        function LoadCategory() {
            $.ajax({
                url: '/api/admin/getAllCategory',
                type: 'GET',
                success: function(rs) {
                    var str = "";
                    $.each(rs,function(i, cate) {
                    	/* str +="<option>";
						str +="<value ='"+cate.id+"'>"+cate.name + "</value>";
						str +="</option>"; */
						str +="<option value='"+cate.id+"'>"+cate.name+"</option>"
								/* $("#getResultDiv").append(
										product.title,product.price); */
							});
                    // id của thẻ tbody để hiển thị tất cả cá tr ra
                    $('#load_data').html(str);
                }
            });
        }
    </script>  -->
    <script type="text/javascript">
    	function addProduct() {
//    		var markupStr = $('#summernote').summernote('code');
    //		alert(markupStr);
   // 		alert($("#summernote").val())
    		let data = {
    				title: $("#exampleInputTitle").val(),
    				price: $("#exampleInputPrice").val(), // lay theo id
    				priceSale: $("#exampleInputPriceSale").val(), // lay theo id
    				shortDescription: $("#exampleInputDescription").val(),
    				details: $("#summernote").val(),
    				categories: {id: $("#category").val()}
    			};
    			// $ === jQuery
    			// json == javascript object
    			$.ajax({
    				url : "/api/admin/product",
    				type : "post",
    				contentType : "application/json",
    				data : JSON.stringify(data),

    				dataType : "json", // kieu du lieu tra ve tu controller la json
    				success : function(result) {
    					//alert("Chuc mung! da luu thanh cong dia chi email " + jsonResult.message.txtLuaChon);
    					alert("Them moi san pham thanh cong");
    					window.location.href = "http://localhost:9999/admin/list-product";
    				},
    				error : function(jqXhr, textStatus, errorMessage) { // error callback 
    					alert("Loi them san pham");
    				}
    			});
		}
    </script>
</html>
